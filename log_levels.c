/*This could be used to create different levels of output for a C program. 
By defining LOG_LEVEL as a number from 0 to 5, one could make a program output messages
from low priority, like LL_VERBOSE_DEBUG to high priority, like LL_ERROR.

So, for example, if LOG_LEVEL was set to 3, the program would output every LL_ERROR, LL_WARNING and LL_INFO messages,
but not LL_DEBUG or LL_VERBOSE_DEBUG messages.

All messages with priority lower to LOG_LEVEL won't impact the program's performance,
not even with a simple check, since their macros will expand to (void)(0), a C no-op.

See the end of file for an example of usage.

*/
#include <stdio.h>

#define JOIN2(x,y) x##y
#define JOIN(x,y) JOIN2(x,y)



#define LL_VERBOSE_DEBUG 5
#define LL_VERBOSE_DEBUG_5(ARGS) (printf("[VERBOSE_DEBUG]"),printf ARGS)
#define LL_VERBOSE_DEBUG_4(ARGS) (void)(0)
#define LL_VERBOSE_DEBUG_3(ARGS) (void)(0)
#define LL_VERBOSE_DEBUG_2(ARGS) (void)(0)
#define LL_VERBOSE_DEBUG_1(ARGS) (void)(0)
#define LL_VERBOSE_DEBUG_0(ARGS) (void)(0)

#define LL_DEBUG 4
#define LL_DEBUG_5(ARGS) (printf("[DEBUG]"),printf ARGS)
#define LL_DEBUG_4(ARGS) LL_DEBUG_5(ARGS)
#define LL_DEBUG_3(ARGS) (void)(0)
#define LL_DEBUG_2(ARGS) (void)(0)
#define LL_DEBUG_1(ARGS) (void)(0)
#define LL_DEBUG_0(ARGS) (void)(0)

#define LL_INFO 3
#define LL_INFO_5(ARGS) (printf("[INFO]"),printf ARGS)
#define LL_INFO_4(ARGS) LL_INFO_5(ARGS)
#define LL_INFO_3(ARGS) LL_INFO_5(ARGS)
#define LL_INFO_2(ARGS) (void)(0)
#define LL_INFO_1(ARGS) (void)(0)
#define LL_INFO_0(ARGS) (void)(0)

#define LL_WARNING 2
#define LL_WARNING_5(ARGS) (printf("[WARNING]"),printf ARGS)
#define LL_WARNING_4(ARGS) LL_WARNING_5(ARGS)
#define LL_WARNING_3(ARGS) LL_WARNING_5(ARGS)
#define LL_WARNING_2(ARGS) LL_WARNING_5(ARGS)
#define LL_WARNING_1(ARGS) (void)(0)
#define LL_WARNING_0(ARGS) (void)(0)

#define LL_ERRROR 1
#define LL_ERROR_5(ARGS) (printf("[ERROR]"),printf ARGS)
#define LL_ERROR_4(ARGS) LL_ERROR_5(ARGS)
#define LL_ERROR_3(ARGS) LL_ERROR_5(ARGS)
#define LL_ERROR_2(ARGS) LL_ERROR_5(ARGS)
#define LL_ERROR_1(ARGS) LL_ERROR_5(ARGS)
#define LL_ERROR_0(ARGS) (void)(0)

#define LOGGING(LEVEL, ARGS) JOIN(LEVEL##_,LOG_LEVEL) (ARGS)





//Usage example:
#define LOG_LEVEL 5 /* output LL_INFO, LL_WARNING and LL_ERROR messages*/

int main(int argc, char **argv)
{
	
	char *my_string = "some useful value";
	
	
	LOGGING(LL_VERBOSE_DEBUG, ("VERBOSE DBG MESSAGE!!!: %s\n", my_string)); /*this won't be printed*/
	LOGGING(LL_DEBUG, ("DBG MESSAGE!!!!: %s\n", my_string)); /*this won't be printed*/
	LOGGING(LL_INFO, ("INFO MESSAGE!!!!: %s\n", my_string)); /*this will be printed*/
	LOGGING(LL_WARNING, ("WARNING MESSAGE!!!!: %s\n", my_string));/*this will be printed*/
	LOGGING(LL_ERROR, ("ERROR MESSAGE!!!: %s\n", my_string));/*this will be printed*/
	
	return 0;
}
	


