Log Levels is a quick C sample with some rather usefull
macros that produce different levels of logging, without disrupting
the flow of the program when that level of logging is not active.

More info in comments within the log_levels.c file itself.
